require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase

  test 'my test' do
    assert_equal('hello world', strip_scripts('hello world', ''))
    assert_equal('<p>hello world</p>', strip_scripts('<p>hello world</p>', ''))
    assert_equal("<img src=\"http://www.smbc-comics.com/comics/20130406.gif\"><br>\nYay it works!", strip_scripts(File.open('test/unit/helpers/smbc.txt').read, ''))

    item_with_youtube_video = File.open('test/unit/helpers/korben_youtube.txt').read
    assert_equal(item_with_youtube_video, strip_scripts(item_with_youtube_video, ''))
  end
end
