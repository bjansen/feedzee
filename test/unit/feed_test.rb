require 'test_helper'

class FeedTest < ActiveSupport::TestCase
  test 'empty url' do
    assert_nil Feed.detect(nil)
  end

  test 'invalid url' do
    assert_nil Feed.detect('not a real url')
    assert_nil Feed.detect('ftp://foo.bar')
  end

  test 'known feed url' do
    assert_equal('XKCD feeds', Feed.detect('http://xkcd.com/rss.xml').name)
  end

  test 'direct feed url' do
    feed = Feed.detect('http://news.samdroid.net/feed/')
    assert_not_nil(feed)
    assert_equal('My feed', feed.name)
    assert_equal('http://my.example.com/rss.xml', feed.url)
  end
end
