var updateInterval;
var elementsToHighlight = [];
var hasFocus;

$(function() {
    updateInterval = setInterval(updateUnreadCount, 60000);

    $(window).focus(function() {
        hasFocus = true;
        highlightElements();
    });

    $(window).blur(function() {
        hasFocus = false;
    });
});

function updateUnreadCount(callback) {
    $("#loading-icon").show();

    $.post('/ajax/countUnread', {}, function(data) {
        elementsToHighlight = [];
        $.each(data.counts, function(id, count) {
            var element = $('#' + id);
            var badge = element.find('> a').find('.badge');
            if (id != 'all_items' && badge.text() != count && !(count == '0' && badge.text() == '')) {
                elementsToHighlight.push(element);
            }
            if (count > '0') {
                badge.text(count).show();
            }
        });
        if (hasFocus) {
            highlightElements();
        }
        ractive.set('all_count_unread', parseInt(data.counts.all_items));
        updateWindowTitle(data.counts.all_items);

        $("#loading-icon").hide();
        typeof callback === 'function' && callback();
    });
}

function highlightElements() {
    $.each(elementsToHighlight, function () {
        $(this).animate({backgroundColor: "#049cdb" }).animate({backgroundColor: "transparent"});
    });
    elementsToHighlight = [];
}
