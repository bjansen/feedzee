var KEY_J = 106, KEY_K = 107, KEY_N = 110, KEY_P = 112, KEY_U = 117, KEY_H = 104;

$(function() {
    $(document).on('keypress', onKeyPress);
});

function onKeyPress(e) {
    var code = (e.keyCode ? e.keyCode : e.which);

    if (code == KEY_J || code == KEY_N) {
        if (firstVisibleEntry) {
            goto(firstVisibleEntry.next('.entry'));
        }
    } else if (code == KEY_K || code == KEY_P) {
        if (firstVisibleEntry) {
            goto(firstVisibleEntry.prev('.entry'));
        }
    } else if (code == KEY_U) {
        clearInterval(updateInterval);
        updateUnreadCount(function() {
            updateInterval = setInterval(updateUnreadCount, 60000);
        });
    } else if (code == KEY_H) {
        $('#kbd-help')
            .modal('toggle', this)
            .one('hide')
    }
}