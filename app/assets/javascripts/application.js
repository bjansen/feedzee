// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree ../../../vendor/assets/javascripts/.
//= require_directory .

var firstVisibleEntry = null;
var skipScrollEvent = false;
var isMobile = false;
var ractive;

var CURRENT_ENTRY_CLASS = 'current-entry';
var UNREAD_CLASS = 'unread';

$(function () {

    isMobile = $(window).width() < 500;

    $.get('rest/feeds', function(data) {
        data.shorten = shorten;
        data.prettyDate = prettyDate;
        data.items = [];
        data.unsuscribable = false;

        ractive = new Ractive({
            el: 'main',
            template: '#mainTemplate',
            data: data
        });

        updateUnreadCount();
        setupListeners(ractive);
    });

    $("#subscribe_btn").click(subscribe);

    $('.badge:empty').hide();
});

function shorten(text) {
    if (text.length > 20) {
        return '<span title="' + text + '" class="with-tooltip">' + text.substring(0, 20) + "...</span>";
    }

    return text;
}

function showTooltips(selector) {
    $(selector).tooltip({
        container: 'body',
        placement: function (a, element) {
            var position = $(element).position();
            if (position.top < 110) {
                return "bottom";
            }
            return "top";
        }
    });
}

function setupListeners(ractive) {
    ractive.on('foldertoggle', toggleFolder);
    ractive.on('foldershow', showItemsInFolder);
    ractive.on('feedshow', showItemsInFeed);
    ractive.on('showall', showAllItems);
    ractive.on('itemclick', selectItem);
    ractive.on('unsubscribe', unsubscribe);
    ractive.on('markallread', markAllAsRead);

    showTooltips('.with-tooltip');

    $(window).resize(function() {
        isMobile = $(window).width() < 500;
    });

    var callbacks = isMobile ? undefined : {
        onScroll: function(){
            $('.tree-top-shadow').css('opacity', Math.abs(mcs.top) / 30);
            $('.tree-bottom-shadow').css('opacity', ($('.mCSB_container').height() + 110 - $('.feeds').height() - Math.abs(mcs.top)) / 15);
        }
    };

    $('.tree').mCustomScrollbar({
        autoHideScrollbar: true,
        scrollInertia: 0,
        callbacks: callbacks
    });

    $('#items').scroll(onWindowScroll);

    $('#show-feeds').click(function() {
        $("#feeds").slideToggle({direction: 'left'});
    });
    $('#prev-item').click(function() {
        if (firstVisibleEntry) {
            goto(firstVisibleEntry.prev('.entry'));
        }
    });
    $('#next-item').click(function() {
        if (firstVisibleEntry) {
            goto(firstVisibleEntry.next('.entry'));
        }
    });
}

function unsubscribe() {
    var feedId = $('li.current-feed').attr('id').substr("feed_".length);

    $.get('ajax/unsubscribe/' + feedId, reset);
}

function reset(data) {
    if (data.folders) {
        ractive.set('folders', data.folders);
        ractive.set('orphans', data.orphans);
    }

    $('.current-feed').removeClass('current-feed');
    ractive.set('current_feed', undefined);
    ractive.set('items', []);
    $('#unsubscribe_popup').modal('hide');
    updateUnreadCount();
}

function selectItem(evt) {
    if (!$(evt.node).hasClass(CURRENT_ENTRY_CLASS)) {
        goto($(evt.node));
    }

    if (evt.original.target.href) { // Links open in new tabs
        evt.original.target.target = '_blank';
    }
}

function toggleFolder(evt) {
    evt.original.stopPropagation();
    var chevron = $(evt.node);
    var branch = chevron.parents('.branch');
    var link = branch.find('> a:first');

    branch.find('ul').slideToggle(function() {
        $('.tree').mCustomScrollbar("update");
    });
    var icon = chevron.find('i');
    icon.toggleClass('glyphicon-chevron-right');
    icon.toggleClass('glyphicon-chevron-down');
    link.blur();

    var folderId = branch.attr('id').substring("folder_".length);
    $.post('/ajax/folder-state', {id: folderId, expanded: icon.hasClass('glyphicon-chevron-down')});
}

function showItemsInFolder(evt) {
    doShowItems(evt, this, 'rest/items/folder/' + evt.context.id, 'folder "' + evt.context.name + '"');
    $("#unsubscribe_btn").attr('disabled', 'disabled');
    $("#markasread_btn").removeAttr('disabled');
}

function showItemsInFeed(evt) {
    doShowItems(evt, this, 'rest/items/feed/' + evt.context.feed_id, 'feed "' + evt.context.feed.name + '"');
    $("#unsubscribe_btn").removeAttr('disabled');
    $("#markasread_btn").removeAttr('disabled');
}

function showAllItems(evt) {
    doShowItems(evt, this, 'rest/items/all', 'all items');
    $("#unsubscribe_btn").attr('disabled', 'disabled');
    $("#markasread_btn").removeAttr('disabled');
}

function doShowItems(evt, ractive, url, feedLabel) {
    $("#loading-icon").show();
    $('.current-feed').removeClass('current-feed');
    $(evt.node).parent().addClass('current-feed');
    ractive.set('current_feed', feedLabel);

    $.get(url, function(data) {
        skipScrollEvent = true;

        $('#items').animate({scrollTop: 0}, 'fast', 'swing', function() {
            if (firstVisibleEntry != null) {
                firstVisibleEntry.removeClass(CURRENT_ENTRY_CLASS);
            }
            ractive.set('items', data, function() {
                $('div.entry').addClass(UNREAD_CLASS);
                $('div.entry-content img').addClass('img-responsive');

                skipScrollEvent = false;

                showTooltips('#items .with-tooltip');
                $("#loading-icon").hide();

                if (isMobile) {
                    $("#feeds").hide();
                }
            });
        });
    });
}

function prettyDate(time){
    var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
        diff = (((new Date()).getTime() - date.getTime()) / 1000),
        day_diff = Math.floor(diff / 86400);

    if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
        return date.toLocaleDateString();

    return day_diff == 0 && (
        diff < 60 && "Just now" ||
            diff < 120 && "A minute ago" ||
            diff < 3600 && Math.floor( diff / 60 ) + " minutes ago" ||
            diff < 7200 && "An hour ago" ||
            diff < 86400 && Math.floor( diff / 3600 ) + " hours ago") ||
        day_diff == 1 && "Yesterday" ||
        day_diff < 7 && day_diff + " days ago" ||
        day_diff < 31 && Math.ceil( day_diff / 7 ) + " weeks ago";
}

function subscribe() {
    var url = $("#feedUrl").val();

    if (url != "") {
        $.post('/ajax/subscribe', {url: url}, function(data) {
            if (data.status == "not found") {
                $("#subscribe_popup").find('span.subscribe_error').text("Could not find any subscription at this URL :(");
            } else if (data.status == "already subscribed") {
                $("#subscribe_popup").find('span.subscribe_error').text("You already subscribed to this feed :D");
            } else {
                window.location.reload();
            }

        });
    }
}

function onWindowScroll(evt) {
    if (skipScrollEvent) {
        return;
    }
    var areaHeight = $(this).height();
    var areaTop = $(window).scrollTop();
    var previousCurrentEntry = firstVisibleEntry;

    firstVisibleEntry = null;

    $('.entry').each(function () {
        var top = $(this).position().top;
        var height = $(this).height();

        if (top + height < areaTop + Math.min($(this).height(), 100)) {
            // above the viewable area
        } else if (top > areaTop + areaHeight) {
            // below the viewable area
        } else {
            if (firstVisibleEntry == null || $(this).offsetTop < firstVisibleEntry.offsetTop) {
                firstVisibleEntry = $(this);
            }
        }
    });

    if (previousCurrentEntry != null) {
        previousCurrentEntry.removeClass(CURRENT_ENTRY_CLASS);
    }

    if (firstVisibleEntry != null) {
        firstVisibleEntry.addClass(CURRENT_ENTRY_CLASS);
    }

    if (firstVisibleEntry.hasClass(UNREAD_CLASS) && firstVisibleEntry != previousCurrentEntry) {
        markAsRead(firstVisibleEntry);
    }
}

function goto(element) {
    if (element == null || element.length == 0) {
        return;
    }

    if (firstVisibleEntry != null) {
        firstVisibleEntry.removeClass(CURRENT_ENTRY_CLASS);
    }
    firstVisibleEntry = element;
    firstVisibleEntry.addClass(CURRENT_ENTRY_CLASS);

    skipScrollEvent = true;
    $('#items').animate({scrollTop: $('#items').scrollTop() + firstVisibleEntry.position().top - 10}, 'fast', 'swing', function() {
        skipScrollEvent = false;
    });

    if (firstVisibleEntry.hasClass(UNREAD_CLASS)) {
        markAsRead(element);
    }
}

// TODO we should use a ractive context instead of a jQuery object, would be easier
function markAsRead(element) {
    var itemId = element.attr('data-item-id');
    element.removeClass(UNREAD_CLASS);

    $.post('/ajax/markAsRead', {id: itemId}, function(data) {

        var feed = $("#feed_" + element.attr('data-feed-id'));
        decreaseCount(feed.find('.badge'));
        decreaseCount(feed.parents('.branch').find('> a').find('.badge'));

        ractive.set('all_count_unread', ractive.get('all_count_unread') - 1);

        updateWindowTitle(ractive.get('all_count_unread'));
    })
        .fail(function() {
            element.addClass(UNREAD_CLASS);

            $('.notifications').notify({
                message: { text: 'Cannot connect to server!' },
                type: 'error'
            }).show();
        });
}

// TODO set the current feed/folder in ractive
function markAllAsRead() {
    var url, params = {};
    var selectedFeed = $('.current-feed').attr('id');

    if (selectedFeed == 'menu') {
        url = 'ajax/markAllAsRead';
    } else {
        var parts = selectedFeed.split('_');
        url = (parts[0] == 'feed') ? 'ajax/markFeedAsRead' : 'ajax/markFolderAsRead';
        params.id = parts[1];
    }

    $.post(url, params, reset);
}

function decreaseCount(element) {
    var newCount = parseInt(element.text()) - 1;

    element.text(newCount);

    if (newCount == 0) {
        element.hide();
    }
}

function updateWindowTitle(numUnread) {
    if (isNaN(numUnread) || numUnread <= 0) {
        $("#favicon").attr("href","/favicon.ico");
        document.title = 'Feedzee';
    } else {
        $("#favicon").attr("href","/favicon-unread.ico");
        document.title = 'Feedzee (' + numUnread + ')';
    }
}
