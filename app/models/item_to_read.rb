class ItemToRead < ActiveRecord::Base
  self.table_name = 'items_to_read'

  attr_accessible :item_id, :user_id

  belongs_to :item
  belongs_to :user

  def self.unread_in_feed(user_id, feed_id)
    self.fetch(ItemToRead.joins(:item).includes(:item).where(:user_id => user_id, 'items.feed_id' => feed_id))
  end

  def self.unread_in_folder(user_id, folder_id)
    self.fetch(ItemToRead.joins(:item => {:feed => :subscriptions}).includes(:item).where(:user_id => user_id, 'subscriptions.folder_id' => folder_id))
  end

  def self.all_unread(user_id)
    self.fetch(ItemToRead.includes(:item).where(:user_id => user_id))
  end

  def self.fetch(data)
    data.order('items.publication_date asc').limit(Rails.configuration.max_items_to_fetch)
  end

  def as_json(opts={})
    item.serializable_hash(:include => :feed)
  end
end
