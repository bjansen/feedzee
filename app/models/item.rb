class Item < ActiveRecord::Base
  attr_accessible :description, :feed_id, :publication_date, :title, :url, :guid, :subscription_pushed

  belongs_to :feed
  has_many :item_to_reads

  def self.items_to_push
    self.connection.select_all('SELECT items.id item_id, sub.user_id user_id FROM items join subscriptions sub on sub.feed_id=items.feed_id WHERE items.subscription_pushed IS NULL')
  end
end
