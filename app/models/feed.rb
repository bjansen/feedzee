class Feed < ActiveRecord::Base
  attr_accessible :last_checked_date, :url, :name, :last_modified_at, :etag, :last_url, :site_url

  has_many :items
  has_many :subscriptions

  ##
  # Finds the Feed corresponding to the given `url`. If the parameter denotes
  # an XML feed, we check that it is valid. If it denotes HTML content, we try to parse it
  # to find <link> tags pointing to the real feed.
  #
  # @param url [String] the URL to analyze
  # @return [Feed] the corresponding Feed, or nil if no valid feed could be detected
  def self.detect(url)
    return unless url =~ /http(s)?:\/\/.*/

    feed = Feed.find_by_url(url)
    return feed if feed

    parsed_feed = Feedjira::Feed.fetch_and_parse(url)

    if parsed_feed
      feed = Feed.new
      feed.name = parsed_feed.title
      feed.url = parsed_feed.feed_url
      feed.site_url = parsed_feed.url

      return feed
    end
  end
end
