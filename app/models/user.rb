class User < ActiveRecord::Base
  attr_accessible :name, :provider, :uid

  has_many :item_to_reads
end
