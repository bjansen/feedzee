class Subscription < ActiveRecord::Base
  attr_accessible :feed_id, :folder_id, :user_id

  belongs_to :feed
  belongs_to :folder
  # TODO redundancy between :user_id in Subscription and in Folder

  def as_json(opts={})
    self.serializable_hash(:include => :feed)
  end
end
