class Folder < ActiveRecord::Base
  attr_accessible :name, :user_id, :is_expanded
  attr_accessor :count_unread

  belongs_to :user
  has_many :subscriptions

  def self.list_user_folders(user_id)
    folders = Folder.includes(:subscriptions => :feed).where(:user_id => user_id)
    orphans = Subscription.includes(:feed).where(:user_id => user_id, :folder_id => nil)

    {:folders => folders, :orphans => orphans}
  end

  # TODO move to Feed?
  def self.count_items_to_read(user_id)
    Feed.group('feeds.id').joins(:items => :item_to_reads).where('items_to_read.user_id' => user_id).count('items_to_read.id')
  end

  def as_json(opts={})
    self.serializable_hash(:include => {:subscriptions => {:include => :feed}})
  end
end
