module ApplicationHelper

  def pretty_date(date)
    a = (Time.now - date).to_i

    case a
      when 0 then
        'just now'
      when 1 then
        'a second ago'
      when 2..59 then
        a.to_s+' seconds ago'
      when 60..119 then
        'a minute ago' #120 = 2 minutes
      when 120..3540 then
        (a/60).to_i.to_s+' minutes ago'
      when 3541..7100 then
        'an hour ago' # 3600 = 1 hour
      when 7101..82800 then
        ((a+99)/3600).to_i.to_s+' hours ago'
      when 82801..172000 then
        'a day ago' # 86400 = 1 day
      when 172001..518400 then
        ((a+800)/(60*60*24)).to_i.to_s+' days ago'
      when 518400..1036800 then
        'a week ago'
      else
        l date
    end
  end

  def shorten_text(text)
    if text.length > 20
      %Q(<span title="#{text}" class="with-tooltip">#{h text[0..20]}...</span>)
    else
      text
    end
  end

  # TODO refactoring, extract config and transformers, pass them parameters (base_url)
  def strip_scripts(text, base_url)
    raw(Sanitize.clean(text, {
        :remove_contents => ['script'],
        :elements => %w[
        a abbr b bdo blockquote br caption cite code col colgroup dd del dfn dl
        dt em figcaption figure h1 h2 h3 h4 h5 h6 hgroup i img ins kbd li mark
        ol p pre q rp rt ruby s samp small strike strong style sub sup table tbody td
        tfoot th thead time tr u ul var wbr
      ],

        :attributes => {
            :all         => ['dir', 'lang', 'title', 'class', 'style'],
            'a'          => ['href'],
            'blockquote' => ['cite'],
            'col'        => ['span', 'width'],
            'colgroup'   => ['span', 'width'],
            'del'        => ['cite', 'datetime'],
            'img'        => ['align', 'alt', 'height', 'src', 'width'],
            'ins'        => ['cite', 'datetime'],
            'ol'         => ['start', 'reversed', 'type'],
            'q'          => ['cite'],
            'table'      => ['summary', 'width'],
            'td'         => ['abbr', 'axis', 'colspan', 'rowspan', 'width'],
            'th'         => ['abbr', 'axis', 'colspan', 'rowspan', 'scope', 'width'],
            'time'       => ['datetime', 'pubdate'],
            'ul'         => ['type']
        },

        :protocols => {
            'a'          => {'href' => ['ftp', 'http', 'https', 'mailto', :relative]},
            'blockquote' => {'cite' => ['http', 'https', :relative]},
            'del'        => {'cite' => ['http', 'https', :relative]},
            'img'        => {'src'  => ['http', 'https', :relative]},
            'ins'        => {'cite' => ['http', 'https', :relative]},
            'q'          => {'cite' => ['http', 'https', :relative]}
        },

        :transformers => [
            lambda do |env|
              node      = env[:node]

              return unless node.element?
              return unless node.has_attribute?('src')
              return unless node.attr('src').start_with?('/')

              node.set_attribute('src', (base_url or '') + node.attr('src'))

              {:node_whitelist => [node]}
            end,

            lambda do |env|
              node      = env[:node]
              node_name = env[:node_name]

              return if env[:is_whitelisted] || !node.element?
              return unless node_name == 'iframe'
              return unless node['src'] =~ /\Ahttps?:\/\/(?:www\.)?youtube(?:-nocookie)?\.com\//

              Sanitize.clean_node!(node, {
                  :elements => %w[iframe],

                  :attributes => {
                      'iframe'  => %w[allowfullscreen frameborder height src width]
                  }
              })

              {:node_whitelist => [node]}
            end
        ]
    }))
  end

end
