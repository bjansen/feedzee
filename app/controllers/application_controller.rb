class ApplicationController < ActionController::Base
  protect_from_forgery

  def check_logged_in
    if session[:current_user_id] == nil
      redirect_to login_url
    else
      add_current_user
    end
  end

  def add_current_user
    @current_user = User.find(session[:current_user_id])
  end

  before_filter :check_logged_in, :except => [:login, :callback]
end
