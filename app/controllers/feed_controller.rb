class FeedController < ApplicationController

  def add

  end

  def index
    prepare_feeds()
  end

  def list_items
    prepare_feeds()

    # TODO add orphan feeds
    @feed = Feed.find(params[:id])
    @items = ItemToRead.unread_in_feed(@current_user.id, @feed.id)
    @current_feed = "feed_#{@feed.id}"
    @current_count_unread = (@items_to_read[@feed.id] or 0)
  end

  def prepare_feeds
    @tree = Folder.list_user_folders(@current_user.id)
    @items_to_read = Folder.count_items_to_read(@current_user.id)
    @all_count_unread = 0

    @tree[:folders].all.each do |folder|
      count = 0

      folder.subscriptions.each do |subscription|
        count += @items_to_read[subscription.feed.id] if @items_to_read[subscription.feed.id]
      end

      folder.count_unread = count
      @all_count_unread = @all_count_unread + count
    end
  end

  def list_folder_items
    prepare_feeds()

    @folder = Folder.find(params[:id])
    @items = ItemToRead.unread_in_folder(@current_user.id, @folder.id)
    @current_feed = "folder_#{@folder.id}"
    @tree[:folders].all.each do |folder|
      @current_count_unread = folder.count_unread if folder.id == @folder.id
    end

    render 'list_items'
  end

  def list_all_items
    prepare_feeds()

    @items = ItemToRead.all_unread(@current_user.id)
    @current_feed = "all_items"
    @current_count_unread = @all_count_unread

    render 'list_items'
  end

  def count_unread
    prepare_feeds()

    render :partial => 'feed/countUnread'
  end
end
