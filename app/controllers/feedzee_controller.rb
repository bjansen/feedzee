class FeedzeeController < ApplicationController

  def index

  end

  def list_feeds
    folders = Folder.list_user_folders(@current_user.id)

    render :json => folders.to_json
  end

  def list_items_in_folder
    items = ItemToRead.unread_in_folder(@current_user.id, params[:id])

    render :json => items.to_json()
  end

  def list_items_in_feed
    items = ItemToRead.unread_in_feed(@current_user.id, params[:id])

    render :json => items.to_json()
  end

  def list_all_items
    items = ItemToRead.all_unread(@current_user.id)

    render :json => items.to_json()
  end

  def change_folder_state
    folder = Folder.find(params[:id])
    folder.update_attribute(:is_expanded, params[:expanded])

    render :json => '{"status": "ok"}'
  end

  def mark_as_read
    ItemToRead.delete_all(:item_id => params[:id], :user_id => @current_user.id)

    render :json => '{"status": "ok"}'
  end

  def mark_all_as_read
    ItemToRead.delete_all(:user_id => @current_user.id)

    render :json => '{"status": "ok"}'
  end

  def mark_feed_as_read
    ItemToRead.where("user_id = ? AND item_id IN (#{Item.select(:id).where(:feed_id => params['id']).to_sql})", @current_user.id).delete_all

    render :json => '{"status": "ok"}'
  end

  def mark_folder_as_read
    ItemToRead.where("user_id = ? AND item_id IN (#{Item.select('items.id').joins(:feed => :subscriptions).where('subscriptions.folder_id' => params['id']).to_sql})", @current_user.id).delete_all

    render :json => '{"status": "ok"}'
  end

  def select_opml
  end

  def import
    uploaded_io = params['opml_file']
    content = uploaded_io.read

    opml = OpmlSaw::Parser.new(content)
    opml.parse

    folders = Hash.new
    imported_feeds_count = 0

    opml.feeds.each do |xml_feed|
      if xml_feed[:tag]
        if folders[xml_feed[:tag]]
          folder = folders[xml_feed[:tag]]
        else
          folder = Folder.where({:name => xml_feed[:tag], :user_id => @current_user.id}).first_or_create!
          folders[xml_feed[:tag]] = folder
        end
      end

      feed = Feed.where(:url => xml_feed[:xml_url]).first_or_create!({:name => xml_feed[:title]})
      Subscription.create({:feed_id => feed.id, :folder_id => folder.id, :user_id => @current_user.id})

      imported_feeds_count += 1
    end

    flash[:notice] = "#{imported_feeds_count} feeds were imported"

    # TODO if feed was already known, add N last items to read
    redirect_to root_url
  end

  def subscribe
    url = params['url']
    feed = Feed.detect(url)

    render :json => '{"status": "not found"}' and return if feed == nil

    feed.save if feed.id == nil

    existing_subscription = Subscription.where(:feed_id => feed.id, :user_id => @current_user.id)

    render :json => '{"status": "already subscribed"}' and return if existing_subscription.exists?

    # TODO add N last items to read (if available)
    subscription = Subscription.new
    subscription.feed = feed
    subscription.user_id = @current_user.id
    subscription.save

    render :json => '{"status": "ok"}'
  end

  def unsubscribe
    feed_id = params['id']
    Subscription.delete_all(:feed_id => feed_id, :user_id => @current_user.id);

    # TODO Should we remove items to read as well?
    render :json => Folder.list_user_folders(@current_user.id).to_json
  end
end
