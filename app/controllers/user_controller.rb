class UserController < ApplicationController

  def callback
    logger.debug(auth_hash)

    user = User.where(:provider => auth_hash.provider, :uid => auth_hash.uid).first

    if user == nil
      user = User.new
      user.provider = auth_hash.provider
      user.uid = auth_hash.uid
      user.save!
    end

    session[:current_user_id] = user.id
    add_current_user

    redirect_to root_url
  end

  def auth_hash
    request.env['omniauth.auth']
  end

  def login

  end

  def logout
    reset_session
    redirect_to root_url
  end
end
