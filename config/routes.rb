Feedzee::Application.routes.draw do
  # The priority is based upon order of creation:
  # first created -> highest priority.

  match 'rest/feeds' => 'feedzee#list_feeds'
  match 'rest/items/all' => 'feedzee#list_all_items'
  match 'rest/items/folder/:id' => 'feedzee#list_items_in_folder'
  match 'rest/items/feed/:id' => 'feedzee#list_items_in_feed'

  match 'feeds/add' => 'feed#add'
  match 'feeds/all' => 'feed#list_all_items', :as => :show_all
  match 'feed/:id' => 'feed#list_items', :as => :show_feed
  match 'folder/:id' => 'feed#list_folder_items', :as => :show_folder

  match 'ajax/folder-state' => 'feedzee#change_folder_state'
  match 'ajax/markAsRead' => 'feedzee#mark_as_read'
  match 'ajax/markAllAsRead' => 'feedzee#mark_all_as_read'
  match 'ajax/markFeedAsRead' => 'feedzee#mark_feed_as_read'
  match 'ajax/markFolderAsRead' => 'feedzee#mark_folder_as_read'
  match 'ajax/subscribe' => 'feedzee#subscribe'
  match 'ajax/unsubscribe/:id' => 'feedzee#unsubscribe'
  match 'ajax/countUnread' => 'feed#count_unread'

  match 'auth/:provider/callback' => 'user#callback'
  match 'login' => 'user#login', :as => :login
  match 'logout' => 'user#logout', :as => :logout

  match 'import' => 'feedzee#select_opml', :as => :select_import, :via => :get
  match 'import' => 'feedzee#import', :as => :import, :via => :post

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'
  root :to => 'feedzee#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
