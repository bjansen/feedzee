**Feed:**

* URL
* lastCheckingDate


**User:**

* name

**Subscription:**

* user_id
* feed_id
* folder_id

**Item:**

* feed_id
* title
* description
* publicationDate
* url

**Folder:**

* Name

**ItemToRead**

* item_id
* user_id