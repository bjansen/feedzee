require 'rss'
require 'open-uri'

class Time
  def to_datetime
    # Convert seconds + microseconds into a fractional number of seconds
    seconds = sec + Rational(usec, 10**6)

    # Convert a UTC offset measured in minutes to one measured in a
    # fraction of a day.
    offset = Rational(utc_offset, 60 * 60 * 24)
    DateTime.new(year, month, day, hour, min, seconds, offset)
  end
end

class FetchItemsTask < Scheduler::SchedulerTask
  environments :all
  # environments :staging, :production
  
  every '2m', {:first_in => '0s', :allow_overlapping => false }

  def run
    log('DEBUG - Starting fetching new items')

    feeds_to_check = Feed.where('last_checked_date is null or last_checked_date < ?', DateTime.now - 1.minute)

    feeds_to_check.each do |feed|
      items = seek_items(feed)

      guids = items.collect{|item| item.guid}
      guids_in_db = Item.select(:guid).where(:guid => guids).collect{|item| item.guid}

      urls = items.collect{|item| item.url}
      urls_in_db = Item.select(:url).where(:url => urls).collect{|item| item.url}

      items_updated = 0

      items.each do |item|
        unless guids_in_db.include?(item.guid) or urls_in_db.include?(item.url)
          item.save!
          items_updated += 1
        end
      end

      log("DEBUG - Added #{items_updated} items from feed #{feed.name}")
    end

    log("INFO - Checked #{feeds_to_check.size} feeds")
  end

  # @param feed[Feed]
  # @return [Array<Item>]
  def seek_items(feed)
    items = []

    log(%(DEBUG - Fetching feed #{feed.name}...\n))

    if feed.last_modified_at && feed.etag
      feedjira = Feedjira::Parser::RSS.new
      feedjira.feed_url = feed.url
      feedjira.etag = feed.etag
      feedjira.last_modified = feed.last_modified_at

      last_entry = Feedjira::Parser::RSSEntry.new
      last_entry.url = feed.last_url

      feedjira.entries << last_entry
      log(%(DEBUG - Using update...\n))
      Feedjira::Feed.update(feedjira)
    else
      log(%(DEBUG - Using fetch_and_parse...\n))
      feedjira = Feedjira::Feed.fetch_and_parse(feed.url)
    end

    if feedjira.respond_to?('entries')
      feedjira.entries.each do |entry|
        item = Item.new()

        item.title = entry.title
        item.description = (entry.content or entry.summary)
        item.feed_id = feed.id
        item.publication_date = if entry.published then entry.published.to_datetime else DateTime.now end
        item.url = entry.url
        item.guid = entry.id

        items.push item
      end

      feed.update_attributes(
          :last_checked_date => DateTime.now,
          :last_modified_at => feedjira.last_modified,
          :etag => feedjira.etag,
          :last_url => feedjira.entries.last.url,
          :site_url => feedjira.url,
          :url => feedjira.feed_url
      )
    else
      log(%(ERROR - Feed #{feed.name}... seems to be invalid, returned response is "#{feedjira}"\n))
    end

    items
  end
end
