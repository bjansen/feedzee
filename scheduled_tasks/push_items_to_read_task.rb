class PushItemsToReadTask < Scheduler::SchedulerTask
  environments :all
  # environments :staging, :production

  every '1m', {:first_in => '0s', :allow_overlapping => false }

  def run
    log('INFO - Starting pushing new items to subscriptions')

    pushed_items = []
    items_to_push = Item.items_to_push

    items_to_push.each do |h|
      ItemToRead.create(:item_id => h['item_id'], :user_id => h['user_id'])
      pushed_items.push(h['item_id']) unless pushed_items.include?(h['item_id'])
    end

    log('INFO - Updated subscriptions')

    pushed_items.each do |item|
      Item.find(item).update_attribute(:subscription_pushed, true)
    end

    log("INFO - #{pushed_items.length} items pushed")
  end
end