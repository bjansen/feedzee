class AddSubscriptionPushedToItem < ActiveRecord::Migration
  def change
    add_column :items, :subscription_pushed, :boolean
  end
end
