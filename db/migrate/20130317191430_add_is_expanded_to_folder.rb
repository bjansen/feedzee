class AddIsExpandedToFolder < ActiveRecord::Migration
  def change
    add_column :folders, :is_expanded, :boolean
  end
end
