class CreateItemsToRead < ActiveRecord::Migration
  def change
    create_table :items_to_read do |t|
      t.integer :item_id
      t.integer :user_id

      t.timestamps
    end
  end
end
