class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.string :url
      t.datetime :last_checked_date

      t.timestamps
    end
  end
end
