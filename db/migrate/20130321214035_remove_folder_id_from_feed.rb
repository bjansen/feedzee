class RemoveFolderIdFromFeed < ActiveRecord::Migration
  def up
    remove_column :feeds, :folder_id
  end

  def down
    add_column :feeds, :folder_id, :integer
  end
end
