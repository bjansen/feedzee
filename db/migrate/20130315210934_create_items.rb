class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :feed_id
      t.string :title
      t.text :description
      t.datetime :publication_date
      t.string :url

      t.timestamps
    end
  end
end
