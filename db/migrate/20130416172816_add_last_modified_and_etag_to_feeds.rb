class AddLastModifiedAndEtagToFeeds < ActiveRecord::Migration
  def change
    add_column :feeds, :last_modified_at, :date
    add_column :feeds, :etag, :string
    add_column :feeds, :last_url, :string
  end
end
