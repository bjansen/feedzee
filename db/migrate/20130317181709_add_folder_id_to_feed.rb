class AddFolderIdToFeed < ActiveRecord::Migration
  def change
    add_column :feeds, :folder_id, :integer
  end
end
