# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([ name: 'Chicago' ,  name: 'Copenhagen' ])
#   Mayor.create(name: 'Emanuel', city: cities.first)

xkcd = Feed.create(url: 'http://xkcd.com/rss.xml', name: 'xkcd.com: A webcomic of romance and math humor.')
smbc = Feed.create(url: 'http://feeds.feedburner.com/smbc-comics/PvLb?format=xml', name: 'Saturday Morning Breakfast Cereal (updated daily)')

folder = Folder.create(name: 'Comics')
Subscription.create(folder_id: folder.id, feed_id: xkcd.id)
Subscription.create(folder_id: folder.id, feed_id: smbc.id)